# Keyboard Heatmap
This repository is fork from [pa7/Keyboard-Heatmap](https://github.com/pa7/Keyboard-Heatmap.git) on GitHub.

Currently support keyboard layouts:
- [Manoonchai 1.0](https://github.com/manoonchai)
- [IKBAEB-th](https://gitlab.com/sahabandha/ikbaeb-th) (Ours (-w-;) )
- Kedmanee TIS-620.2538
- Kedmanee TIS-620.2536
- Kedmanee Rheinmetall
- Kedmanee N/A 2533 (Found in กลุ้มใจ อัสนี - วสันต์ 2533 MV; It may be the origin of Kasemanee.)
- Pattajoti X11
- Pattajoti Nectec
- Pattajoti Thanakan
- Underwood
- McFARLAND

## Want more?
A keyboard layout that you create by yourself?

Old typewriter in memory?

Whatever?

Leave it on the issue.

All welcome. （⌒▽⌒ゞ

## License
The Keyboard Heatmap is licensed under the [MIT](http://www.opensource.org/licenses/mit-license.php "") and [Beerware](http://en.wikipedia.org/wiki/Beerware "") License.

## Contact
If you have questions to ask original creater on whatever digital medium you prefer.
- [@patrickwied](http://twitter.com/#!/patrickwied "on twitter")
- [www.patrick-wied.at](http://www.patrick-wied.at "on his website")

But for Thai layouts.
- [@s2hanano](https://pleroma.in.th/s2hanano "on Thai-Pleroma Fediverse") on Thai-Pleroma Fediverse.

Try it here:https://sahabandha.gitlab.io/Thai-Keyboard-Heatmap
